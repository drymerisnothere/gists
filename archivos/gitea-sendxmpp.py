#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import xmpp
import json
from flask import Flask,request
import ConfigParser


app = Flask(__name__)

@app.route("/webhook", methods=['POST'])
def webhook():
    # read config file
    config = ConfigParser.ConfigParser()
    config.read('config.ini')
    username = config.get('xmpp', 'username', 0)
    password = config.get('xmpp', 'password', 0)
    server = config.get('xmpp', 'server', 0)
    room = config.get('xmpp', 'room', 0)
    nick = config.get('xmpp', 'nick', 0)
    secret = config.get('gitea', 'secret', 0)

    # TODO: comprobar si es cierto
    if not secret:
        secret = ''

    if request.json['secret'] == secret:
        data = json.loads(request.data.decode('utf-8'))
        message = ''
        print(data)

        # commit
        if 'compare_url' in data.keys():
            message = data['pusher']['username'] + ' has pushed some changes: ' + \
                      data['compare_url']
        # pull request open
        elif data['action'] == 'opened':
            message = data['sender']['username'] + ' opened a new pull request: ' + \
                      data['pull_request']['html_url']
        # close pull request
        elif data['action'] == 'closed':
            message = data['sender']['username'] + ' closed a pull request: ' + \
                      data['pull_request']['html_url']
        # reopen pull request
        elif data['action'] == 'reopened':
            message = data['sender']['username'] + ' reopened a pull request: ' + \
                      data['pull_request']['html_url']
        # add label
        elif data['action'] == 'label_updated':
            f_tag = ""
            for tag in data['pull_request']['labels']:
                f_tag += '[' + tag['name'] + '] '
            message = data['sender']['username'] + ' changed the labels ' \
                      'of a pull request: ' + f_tag + \
                      data['pull_request']['html_url']
        # delete label
        elif data['action'] == 'label_cleared':
            message = data['sender']['username'] + ' deleted all the labels ' \
                      'of a pull request: ' + data['pull_request']['html_url']

        if message:
            client = xmpp.Client(server, debug=[])
            client.connect()
            client.auth(username, password, 'gitea')

            # send join
            client.send(xmpp.Presence(to="%s/%s" % (room, nick)))

            msg = xmpp.protocol.Message(body=message)
            msg.setTo(room)
            msg.setType('groupchat')

            client.send(msg)
            presence = xmpp.Presence(to=room)
            presence.setAttr('type', 'unavailable')
            client.send(presence)

    return ":)"


if __name__ == "__main__":

    app.run()
