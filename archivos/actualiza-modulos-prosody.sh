#!/bin/bash

MODULES=("mod_http_upload" "mod_smacks" "mod_mam" "mod_blocking" "mod_carbons" "mod_onions" "mod_csi" "mod_limit_auth" "mod_default_bookmarks")
MODULES_PATH="/usr/lib/prosody/modules/"

hg pull
hg update

for mod in ${MODULES[@]}
do
    cp -r $mod $MODULES_PATH
done

chown -R prosody.prosody $MODULES_PATH
