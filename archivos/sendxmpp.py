#!/usr/bin/env python

import xmpp
from os import sys as sys
import time

username = ''
passwd = ''
server = ''

file_name = sys.argv[1]
to = sys.argv[2]
file = open(file_name, 'r')
msg = file.read()
client = xmpp.Client(server, debug=[]) # poner debug en True si tal
client.connect()
client.auth(username, passwd, 'Mensajero')
client.sendInitPresence()
message = xmpp.Message(to, msg)
message.setAttr('type', 'chat')
client.send(message)
time.sleep(3) # Si se desconecta demasiad rápido, no envia el mensaje
