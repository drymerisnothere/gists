#!/bin/bash

user=""
password=""
gitea=""
users=$(curl --silent -u $user:$password $gitea | grep "User Manage Panel" | cut -d':' -f2 | cut -d')' -f1 | sed "s/^ //")
recipient=""

if [[ ! -d ~/.gitea/ ]]; then
    mkdir ~/.gitea/
    echo $users > ~/.gitea/users

else
    old_users=$(cat ~/.gitea/users)

    if [[ $users != $old_users ]]
    then
	echo "Hay al menos un nuevo usuario en Gitea. Antes habian $old_users y ahora hay $users." >> "/tmp/users"
	sendxmpp /tmp/users $recipient
	rm /tmp/users
    fi
fi
