

# Índice     :TOC_5_org:

-   [Prosody](#org09538d6)
    -   [Actualizar módulos que no forman parte del núcleo](#org207e8c5)
    -   [Archivo de configuración de prosoy de daemons.it](#org90fed92)
-   [Gitea](#org3e31091)
    -   [Avisos de nuevos usuarios de gitea](#orgbafa553)
    -   [Webhook](#org037c36d)
-   [Sendxmpp](#orgeca98b8)


<a id="org09538d6"></a>

# Prosody


<a id="org207e8c5"></a>

## Actualizar módulos que no forman parte del núcleo

    #!/bin/bash

    MODULES=("mod_http_upload" "mod_smacks" "mod_mam" "mod_blocking" "mod_carbons" "mod_onions" "mod_csi" "mod_limit_auth" "mod_default_bookmarks")
    MODULES_PATH="/usr/lib/prosody/modules/"

    hg pull
    hg update

    for mod in ${MODULES[@]}
    do
        cp -r $mod $MODULES_PATH
    done

    chown -R prosody.prosody $MODULES_PATH

En `MODULES` se concreta que módulos del [repositorio](http://modules.prosody.im/) que no forma parte del núcleo se quieren actualizar y `modules_path` es la ruta de estos.


<a id="org90fed92"></a>

## Archivo de configuración de prosoy de daemons.it

    -- Prosody XMPP Server Configuration
    --
    -- Information on configuring Prosody can be found on our
    -- website at http://prosody.im/doc/configure
    --
    -- Tip: You can check that the syntax of this file is correct
    -- when you have finished by running: luac -p prosody.cfg.lua
    -- If there are any errors, it will let you know what and where
    -- they are, otherwise it will keep quiet.
    --
    -- Good luck, and happy Jabbering!


    ---------- Server-wide settings ----------
    -- Settings in this section apply to the whole server and are the default settings
    -- for any virtual hosts

    -- This is a (by default, empty) list of accounts that are admins
    -- for the server. Note that you must create the accounts separately
    -- (see http://prosody.im/doc/creating_accounts for info)
    -- Example: admins = { "user1@example.com", "user2@example.net" }
    admins = { "drymer@daemons.it" }

    -- Enable use of libevent for better performance under high load
    -- For more information see: http://prosody.im/doc/libevent
    --use_libevent = true;

    -- interfaces
    interfaces = { "127.0.0.1", "185.101.93.221" }

    -- This is the list of modules Prosody will load on startup.
    -- It looks for mod_modulename.lua in the plugins folder, so make sure that exists too.
    -- Documentation on modules can be found at: http://prosody.im/doc/modules
    modules_enabled = {
    	-- Generally required
    		"roster"; -- Allow users to have a roster. Recommended ;)
    		"saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
    		"tls"; -- Add support for secure TLS on c2s/s2s connections
    		"dialback"; -- s2s dialback support
    		"disco"; -- Service discovery
    		"posix"; -- POSIX functionality, sends server to background, enables syslog, etc.

    	-- Not essential, but recommended
    		"private"; -- Private XML storage (for room bookmarks, etc.)
    		"vcard"; -- Allow users to set vCards

    	-- These are commented by default as they have a performance impact
    		"privacy"; -- Support privacy lists
    		-- "compression"; -- Stream compression (requires the lua-zlib package installed)

    	-- Nice to have
    		"version"; -- Replies to server version requests
    		"uptime"; -- Report how long server has been running
    		"time"; -- Let others know the time here on this server
    		"ping"; -- Replies to XMPP pings with pongs
    		"pep"; -- Enables users to publish their mood, activity, playing music and more
    		--"register"; -- Allow users to register on this server using a client and change passwords

    	-- Admin interfaces
    		--"admin_adhoc"; -- Allows administration via an XMPP client that supports ad-hoc commands
    		--"admin_telnet"; -- Opens telnet console interface on localhost port 5582

    	-- HTTP modules
    		--"bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
    		--"http_files"; -- Serve static files from a directory over HTTP

    	-- Other specific functionality
    		--"groups"; -- Shared roster support
    		"announce"; -- Send announcement to all online users
    		"welcome"; -- Welcome users who register accounts
    		"watchregistrations"; -- Alert admins of registrations
    		"motd"; -- Send a message to users when they log in
                    "smacks";
                    "csi";
                    "throttle_presence";
                    "filter_chatstates";
                    "mam";
                    "onions";
                    "carbons";
    		"blocking";
                    "limit_auth";
                    "default_bookmarks";
    };

    -- Disable account creation by default, for security
    -- For more information see http://prosody.im/doc/creating_accounts
    allow_registration = false;

    -- Force clients to use encrypted connections? This option will
    -- prevent clients from authenticating unless they are using encryption.

    c2s_require_encryption = true;

    -- Force certificate authentication for server-to-server connections?
    -- This provides ideal security, but requires servers you communicate
    -- with to support encryption AND present valid, trusted certificates.
    -- NOTE: Your version of LuaSec must support certificate verification!
    -- For more information see http://prosody.im/doc/s2s#security

    s2s_secure_auth = false;

    -- Many servers don\'t support encryption or have invalid or self-signed
    -- certificates. You can list domains here that will not be required to
    -- authenticate using certificates. They will be authenticated using DNS.

    -- s2s_insecure_domains = {"xmpp.elbinario.net", "salas.xmpp.elbinario.net", }

    -- Even if you leave s2s_secure_auth disabled, you can still require valid
    -- certificates for some domains by specifying a list here.

    --s2s_secure_domains = { "jabber.org" }

    -- Required for init scripts and prosodyctl
    pidfile = "/var/run/prosody/prosody.pid"

    -- Select the authentication backend to use. The 'internal' providers
    -- use Prosody\'s configured data storage to store the authentication data.
    -- To allow Prosody to offer secure authentication mechanisms to clients, the
    -- default provider stores passwords in plaintext. If you do not trust your
    -- server please see http://prosody.im/doc/modules/mod_auth_internal_hashed
    -- for information about using the hashed backend.

    authentication = "internal_hashed"

    -- Logging configuration
    -- For advanced logging see http://prosody.im/doc/logging
    log = {
    	-- info = "/var/log/prosody/prosody.log"; -- Change 'info' to 'debug' for verbose logging
    	error = "/var/log/prosody/prosody.err";
            -- debug = "/var/log/prosody/prosody.debug";
    }

    ssl = {
    	dhparam = "/etc/nginx/ssl/dh-4096.pem";
            certificate = "/etc/prosody/certs/daemons.cf/daemons.cf.fullchain";
            key = "/etc/prosody/certs/daemons.cf/daemons.cf.privkey";
            options = { "no_sslv2", "no_sslv3", "no_tlsv1", "no_ticket", "no_compression", "cipher_server_preference", "single_dh_use", "single_ecdh_use" }
    }

    -- Tor
    onions_tor_all = true
    onions_only = false
    onions_map = {
    	["taolo.ga"] = "l3ybpw4vs6ie5rv2.onion";
    	["jabber.calyxinstitute.org"] = "ijeeynrc6x2uy5ob.onion";
    	["riseup.net"] = "4cjw6cwpeaeppfqz.onion";
    	["jabber.otr.im"] = "5rgdtlawqkcplz75.onion";
    	["jabber.systemli.org"] = "x5tno6mwkncu5m3h.onion";
    	["securejabber.me"] = "giyvshdnojeivkom.onion";
    	["so36.net"] = "s4fgy24e2b5weqdb.onion";
    	["autistici.org"] = "wi7qkxyrdpu5cmvr.onion";
    	["inventati.org"] = "wi7qkxyrdpu5cmvr.onion";
    	["jabber.ipredator.se"] = "3iffdebkzzkpgipa.onion";
    	["cloak.dk"] = "m2dsl4banuimpm6c.onion";
    	["im.koderoot.net"] = "ihkw7qy3tok45dun.onion";
    	["anonymitaet-im-inter.net"] = "rwf5skuv5vqzcdit.onion";
    	["jabber.ccc.de"] = "okj7xc6j2szr2y75.onion";
    }

    -- MAM
    archive_expires_after = "1w"

    -- HTTP Upload
    https_ssl = {
           certificate = "/etc/prosody/certs/daemons.it/imagenes.daemons.it.fullchain";
           key = "/etc/prosody/certs/daemons.it/imagenes.daemons.it.privkey";
    }

    http_upload_file_size_limit = 4096
    http_external_url = "https://imagenes.daemons.it"
    Component "imagenes.daemons.it" "http_upload"

    -- limit_auth
    limit_auth_period = 30 -- over 30 seconds
    limit_auth_max = 5 -- tolerate no more than 5 failed attempts

    -- MUC
    Component "salas.daemons.cf" "muc"
        name = "Salas de Bad Daemons"
    Component "salas.daemons.it" "muc"
        name = "Salas de Bad Daemons"
    Component "salas.daemon4jidu2oig6.onion" "muc"
        name = "Salas de Bad Daemons"

    -- Default MuC
    default_bookmarks = {
        { jid = "daemons@salas.daemons.it", name = "Bad Daemons" };
    };

    -- Welcome
    welcome_message = "Bienvenida a este servidor, $user. Para que vea que no eres un bot, saluda en la sala daemons@salas.daemons.it. Solo tienes que hacerlo una vez y luego puedes borrar la sala. Si no lo haces, es posible que borre esta cuenta en un plazo de una semana. Saludos"

    ----------- Virtual hosts -----------
    -- You need to add a VirtualHost entry for each domain you wish Prosody to serve.
    -- Settings under each VirtualHost entry apply *only* to that host.

    VirtualHost "daemons.cf"

    VirtualHost "daemon4jidu2oig6.onion"

    VirtualHost "daemons.it"
    ssl = {
    	dhparam = "/etc/nginx/ssl/dh-4096.pem";
            certificate = "/etc/prosody/certs/daemons.it/daemons.it.fullchain";
            key = "/etc/prosody/certs/daemons.it/daemons.it.privkey";
            options = { "no_sslv2", "no_sslv3", "no_tlsv1", "no_ticket", "no_compression", "cipher_server_preference", "single_dh_use", "single_ecdh_use" }
    }


<a id="org3e31091"></a>

# Gitea


<a id="orgbafa553"></a>

## Avisos de nuevos usuarios de gitea

Avisa por xmpp si se registran nuevos usuarios en gitea. Usa <a id="org12ab14e"></a>.

    #!/bin/bash

    user=""
    password=""
    gitea=""
    users=$(curl --silent -u $user:$password $gitea | grep "User Manage Panel" | cut -d':' -f2 | cut -d')' -f1 | sed "s/^ //")
    recipient=""

    if [[ ! -d ~/.gitea/ ]]; then
        mkdir ~/.gitea/
        echo $users > ~/.gitea/users

    else
        old_users=$(cat ~/.gitea/users)

        if [[ $users != $old_users ]]
        then
    	echo "Hay al menos un nuevo usuario en Gitea. Antes habian $old_users y ahora hay $users." >> "/tmp/users"
    	sendxmpp /tmp/users $recipient
    	rm /tmp/users
        fi
    fi


<a id="org037c36d"></a>

## Webhook

Una aplicación en flask que monta un endpoint en el que recibe peticiones del webhook y las manda por xmpp a una sala.

El script en python2:

    #!/usr/bin/env python2
    # -*- coding: utf-8 -*-

    import xmpp
    import json
    from flask import Flask, request
    import ConfigParser


    app = Flask(__name__)


    @app.route('/')
    def root():
        return redirect("/webhook", code=302)

    @app.route("/webhook", methods=['POST'])
    def webhook():
        # read config file
        config = ConfigParser.ConfigParser()
        config.read('config.ini')
        username = config.get('xmpp', 'username', 0)
        password = config.get('xmpp', 'password', 0)
        server = config.get('xmpp', 'server', 0)
        room = config.get('xmpp', 'room', 0)
        nick = config.get('xmpp', 'nick', 0)
        secret = config.get('gitea', 'secret', 0)

        # TODO: comprobar si funciona sin secret
        if not secret:
            secret = ''

        if request.json['secret'] == secret:
            data = json.loads(request.data.decode('utf-8'))
            message = ''
            print(data)

            # commit
            if 'compare_url' in data.keys():
                message = data['pusher']['username'] + ' has pushed some changes:'\
                          + ' ' + data['compare_url']
            # pull request open
            elif data['action'] == 'opened':
                message = data['sender']['username'] + ' opened a new pull reques'\
                          + 't: ' + data['pull_request']['html_url']
            # close pull request
            elif data['action'] == 'closed':
                message = data['sender']['username'] + ' closed a pull request: ' \
                          + data['pull_request']['html_url']
            # reopen pull request
            elif data['action'] == 'reopened':
                message = data['sender']['username'] + ' reopened a pull request:'\
                          + ' ' + data['pull_request']['html_url']
            # add label
            elif data['action'] == 'label_updated':
                f_tag = ""
                for tag in data['pull_request']['labels']:
                    f_tag += '[' + tag['name'] + '] '
                message = data['sender']['username'] + ' changed the labels ' \
                               'of a pull request: ' + f_tag + \
                               data['pull_request']['html_url']
            # delete label
            elif data['action'] == 'label_cleared':
                message = data['sender']['username'] + ' deleted all the labels ' \
                          'of a pull request: ' + data['pull_request']['html_url']

            if message:
                client = xmpp.Client(server, debug=[])
                client.connect()
                client.auth(username, password, 'gitea')

                # send join
                client.send(xmpp.Presence(to="%s/%s" % (room, nick)))

                msg = xmpp.protocol.Message(body=message)
                msg.setTo(room)
                msg.setType('groupchat')

                client.send(msg)
                presence = xmpp.Presence(to=room)
                presence.setAttr('type', 'unavailable')
                client.send(presence)

        return ":)"


    if __name__ == "__main__":

        app.run()

Necesita un archivo de configuración en el mismo escritorio:

    [xmpp]
    username =
    password =
    server   =
    room     =
    nick     =

    [gitea]
    secret   =

La configuración de uwsgi:

    [uwsgi]

    chdir = /var/www/gitea-sendxmpp/
    module = gitea-sendxmpp:app

    master = true
    processes = 1
    threads = 2

    uid = www-data
    gid = www-data
    socket = /tmp/gitea-sendxmpp.sock
    chmod-socket = 777

    die-on-term = true

La configuración de nginx:

    server {
        listen 80;
        server_name daemons.it;

        location / {
            include uwsgi_params;
            uwsgi_pass unix:/tmp/gitea-sendxmpp.sock;
        }
    }


<a id="orgeca98b8"></a>

# Sendxmpp

<a id="org5f4534d"></a>
Lee un fichero que se le pase como primer parámetro y lo envía al jid que se pase como segundo parámetro.

    #!/usr/bin/env python

    import xmpp
    from os import sys as sys
    import time

    username = ''
    passwd = ''
    server = ''

    file_name = sys.argv[1]
    to = sys.argv[2]
    file = open(file_name, 'r')
    msg = file.read()
    client = xmpp.Client(server, debug=[]) # poner debug en True si tal
    client.connect()
    client.auth(username, passwd, 'Mensajero')
    client.sendInitPresence()
    message = xmpp.Message(to, msg)
    message.setAttr('type', 'chat')
    client.send(message)
    time.sleep(3) # Si se desconecta demasiad rápido, no envia el mensaje
